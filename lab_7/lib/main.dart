// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Stu-Do-List',
        theme: ThemeData(fontFamily: 'Raleway'),
        home: Scaffold(
          appBar: AppBar(
            title: Text('Stu-Do-List',
                style: TextStyle(color: Colors.grey.shade200)),
            backgroundColor: Colors.black87,
          ),
          body: FormKomunitas(),
        ));
  }
}

class FormKomunitas extends StatelessWidget {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  FormKomunitas({Key? key}) : super(key: key);

  TextEditingController matkul = TextEditingController();
  TextEditingController kelas = TextEditingController();
  TextEditingController prodi = TextEditingController();
  TextEditingController info = TextEditingController();
  TextEditingController link = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 40, right: 40),
      child: Form(
        key: formKey,
        child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
          SizedBox(height: 20),
          Text("Tambah Komunitas Belajar",
              style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold)),
          SizedBox(height: 20),
          TextFormField(
              controller: matkul,
              autofocus: true,
              decoration: InputDecoration(
                  hintText: "Masukkan nama mata kuliah",
                  labelText: "Nama Mata Kuliah ",
                  border: OutlineInputBorder()),
              keyboardType: TextInputType.text,
              validator: (value) {
                if (value!.isEmpty) {
                  return "Nama mata kuliah tidak boleh kosong";
                }
              }),
          SizedBox(height: 16),
          TextFormField(
              controller: kelas,
              decoration: InputDecoration(
                hintText: "Masukkan kelas",
                labelText: "Kelas ",
                border: OutlineInputBorder(),
              ),
              keyboardType: TextInputType.text,
              validator: (value) {
                if (value!.isEmpty) {
                  return "Kelas tidak boleh kosong";
                }
              }),
          SizedBox(height: 16),
          TextFormField(
              controller: prodi,
              decoration: InputDecoration(
                hintText: "Masukkan program studi",
                labelText: "Program Studi ",
                border: OutlineInputBorder(),
              ),
              keyboardType: TextInputType.text,
              validator: (value) {
                if (value!.isEmpty) {
                  return "Program studi tidak boleh kosong";
                }
              }),
          SizedBox(height: 16),
          TextFormField(
              controller: info,
              decoration: InputDecoration(
                hintText: "Masukkan keterangan tambahan",
                labelText: "Keterangan ",
                border: OutlineInputBorder(),
              ),
              keyboardType: TextInputType.text,
              validator: (value) {
                if (value!.isEmpty) {
                  return "Keterangan tidak boleh kosong";
                }
              }),
          SizedBox(height: 16),
          TextFormField(
              controller: link,
              decoration: InputDecoration(
                hintText: "Masukkan situs dalam bentul URL",
                labelText: "Situs Grup ",
                border: OutlineInputBorder(),
              ),
              keyboardType: TextInputType.text,
              validator: (value) {
                if (value!.isEmpty) {
                  return "Situs tidak boleh kosong";
                }
              }),
          SizedBox(height: 20),
          ButtonBar(
            buttonPadding: const EdgeInsets.only(right: 24, bottom: 24),
            alignment: MainAxisAlignment.center,
            children: [
              OutlinedButton(
                  child: Text("Batal"),
                  onPressed: () {},
                  style: OutlinedButton.styleFrom(
                      primary: Colors.red,
                      side: BorderSide(color: Colors.red, width: 1))),
              ElevatedButton(
                  onPressed: () {
                    if (formKey.currentState!.validate()) {
                      showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                                content: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(matkul.text),
                                Text(kelas.text),
                                Text(prodi.text),
                                Text(info.text),
                                Text(link.text)
                              ],
                            ));
                          });
                    }
                  },
                  child: Text('Kirim'),
                  style: ElevatedButton.styleFrom(
                      primary: Colors.blueAccent.shade400))
            ],
          ),
        ]),
      ),
    );
  }
}
