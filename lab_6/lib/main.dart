import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:url_launcher/url_launcher.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stu-Do-List',
      theme: ThemeData(
          fontFamily: 'Raleway',
          scaffoldBackgroundColor: Colors.blueGrey.shade50),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Stu-Do-List',
              style: TextStyle(color: Colors.grey.shade200)),
          backgroundColor: Colors.black87,
        ),
        body: KomunitasBelajar(),
      ),
    );
  }
}

class KomunitasBelajar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(children: [
      Card(
        child: Container(
          height: 250,
          // width: double.infinity,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(40),
                  bottomLeft: Radius.circular(40)),
              image: DecorationImage(
                  fit: BoxFit.cover,
                  colorFilter: ColorFilter.mode(
                      Colors.black.withOpacity(0.5), BlendMode.darken),
                  image: NetworkImage(
                      'https://cdnb.artstation.com/p/assets/images/images/024/858/699/original/pixel-jeff-divoom.gif?1583771904'))),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ListTile(
                title: Text('Komunitas Belajar Stu-Do-List',
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.w700)),
                subtitle: Text(
                    'Lihat atau tambahkan komunitas belajar dari mata kuliah yang kamu ikuti di sini.',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                      fontStyle: FontStyle.italic,
                    )),
              ),
              ButtonBar(
                buttonPadding: const EdgeInsets.only(left: 24),
                alignment: MainAxisAlignment.start,
                children: [
                  ElevatedButton(onPressed: () {}, child: Text('Tambah'))
                ],
              )
            ],
          ),
        ),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(40),
                bottomLeft: Radius.circular(40))),
        margin: EdgeInsets.only(left: 0, right: 0, top: 0),
      ),
      SizedBox(height: 16),
      Card(
        margin: const EdgeInsets.symmetric(horizontal: 14),
        shape: RoundedRectangleBorder(
          side: BorderSide(color: Colors.grey.shade300, width: 1),
          borderRadius: BorderRadius.circular(8),
        ),
        child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
          ListTile(
            title: Text('MPPI', style: TextStyle(fontSize: 14)),
            tileColor: Colors.grey.shade100,
            contentPadding:
                EdgeInsets.symmetric(vertical: 0.0, horizontal: 16.0),
          ),
          Container(
            padding: const EdgeInsets.only(top: 16.0, left: 16.0),
            alignment: Alignment.centerLeft,
            child: Text('A, B, C, D, E, F, G',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14)),
          ),
          Container(
            padding: const EdgeInsets.only(left: 16),
            alignment: Alignment.centerLeft,
            child: Text('Sistem Informasi & Ilmu Komputer',
                style: TextStyle(color: Colors.grey, fontSize: 12)),
          ),
          Container(
            padding: const EdgeInsets.only(top: 8, left: 16),
            alignment: Alignment.centerLeft,
            child: Text(
              'Grup LINE dengan asdos',
              style: TextStyle(
                  color: Colors.black,
                  fontStyle: FontStyle.italic,
                  fontSize: 14),
            ),
          ),
          Container(
            padding: const EdgeInsets.only(top: 8, left: 16, bottom: 16),
            alignment: Alignment.centerLeft,
            child: InkWell(
                child: Text(
                  'https://line.me/R/ti/g/MwnZ8GaMYk',
                  textAlign: TextAlign.left,
                  style: TextStyle(color: Colors.blue, fontSize: 14),
                ),
                onTap: () => launch('https://line.me/R/ti/g/MwnZ8GaMYk')),
          ),
          ButtonBar(
            buttonPadding: const EdgeInsets.only(right: 24, bottom: 24),
            alignment: MainAxisAlignment.end,
            children: [
              ElevatedButton(
                  onPressed: () {},
                  child: Text('Hapus'),
                  style: ElevatedButton.styleFrom(primary: Colors.red.shade900))
            ],
          ),
        ]),
      ),
      SizedBox(height: 10),
      Card(
        margin: const EdgeInsets.symmetric(horizontal: 14),
        shape: RoundedRectangleBorder(
          side: BorderSide(color: Colors.grey.shade300, width: 1),
          borderRadius: BorderRadius.circular(8),
        ),
        child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
          ListTile(
            title: Text('SDA', style: TextStyle(fontSize: 14)),
            tileColor: Colors.grey.shade100,
            contentPadding:
                EdgeInsets.symmetric(vertical: 0.0, horizontal: 16.0),
          ),
          Container(
            padding: const EdgeInsets.only(top: 16, left: 16.0),
            alignment: Alignment.centerLeft,
            child: Text('A, B, C, D, E, F',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14)),
          ),
          Container(
            padding: const EdgeInsets.only(left: 16.0),
            alignment: Alignment.centerLeft,
            child: Text('Sistem Informasi & Ilmu Komputer',
                style: TextStyle(color: Colors.grey, fontSize: 12)),
          ),
          Container(
            padding: const EdgeInsets.only(top: 8, left: 16.0),
            alignment: Alignment.centerLeft,
            child: Text(
              'Discord dengan asdos dan dosen',
              style: TextStyle(
                  color: Colors.black,
                  fontStyle: FontStyle.italic,
                  fontSize: 14),
            ),
          ),
          Container(
            padding: const EdgeInsets.only(top: 8, left: 16, bottom: 16),
            alignment: Alignment.centerLeft,
            child: InkWell(
                child: Text(
                  'https://line.me/R/ti/g/MwnZ8GaMYk',
                  textAlign: TextAlign.left,
                  style: TextStyle(color: Colors.blue, fontSize: 14),
                ),
                onTap: () => launch('https://line.me/R/ti/g/MwnZ8GaMYk')),
          ),
          ButtonBar(
            buttonPadding: const EdgeInsets.only(right: 24, bottom: 24),
            alignment: MainAxisAlignment.end,
            children: [
              ElevatedButton(
                  onPressed: () {},
                  child: Text('Hapus'),
                  style: ElevatedButton.styleFrom(primary: Colors.red.shade900))
            ],
          ),
        ]),
      ),
      SizedBox(height: 10),
      Card(
        margin: const EdgeInsets.symmetric(horizontal: 14),
        shape: RoundedRectangleBorder(
          side: BorderSide(color: Colors.grey.shade300, width: 1),
          borderRadius: BorderRadius.circular(8),
        ),
        child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
          ListTile(
            title: Text('Matematika Diskret 1', style: TextStyle(fontSize: 14)),
            tileColor: Colors.grey.shade100,
            contentPadding:
                EdgeInsets.symmetric(vertical: 0.0, horizontal: 16.0),
          ),
          Container(
            padding: const EdgeInsets.only(top: 16, left: 16.0),
            alignment: Alignment.centerLeft,
            child: Text('B',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14)),
          ),
          Container(
            padding: const EdgeInsets.only(left: 16.0),
            alignment: Alignment.centerLeft,
            child: Text('Sistem Informasi & Ilmu Komputer',
                style: TextStyle(color: Colors.grey, fontSize: 12)),
          ),
          Container(
            padding: const EdgeInsets.only(top: 8, left: 16.0),
            alignment: Alignment.centerLeft,
            child: Text(
              'Grup WhatsApp dengan dosen',
              style: TextStyle(
                  color: Colors.black,
                  fontStyle: FontStyle.italic,
                  fontSize: 14),
            ),
          ),
          Container(
            padding: const EdgeInsets.only(top: 8, left: 16, bottom: 16),
            alignment: Alignment.centerLeft,
            child: InkWell(
                child: Text(
                  'https://line.me/R/ti/g/MwnZ8GaMYk',
                  textAlign: TextAlign.left,
                  style: TextStyle(color: Colors.blue, fontSize: 14),
                ),
                onTap: () => launch('https://line.me/R/ti/g/MwnZ8GaMYk')),
          ),
          ButtonBar(
            buttonPadding: const EdgeInsets.only(right: 24, bottom: 24),
            alignment: MainAxisAlignment.end,
            children: [
              ElevatedButton(
                  onPressed: () {},
                  child: Text('Hapus'),
                  style: ElevatedButton.styleFrom(primary: Colors.red.shade900))
            ],
          ),
        ]),
      ),
      SizedBox(height: 10),
    ]);
  }
}
