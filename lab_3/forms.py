from django import forms
from lab_1.models import Friend
from django.forms.widgets import NumberInput

class FriendForm(forms.ModelForm):
    class Meta: 
        model = Friend
        fields = "__all__"
    dob = forms.DateField(widget=NumberInput(attrs={'type':'date'}))