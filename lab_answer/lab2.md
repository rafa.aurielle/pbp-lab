1. XML merupakan markup language yang digunakan untuk menyimpan serta mengatur pengiriman data, sedangkan JSON merupakan format untuk penyimpanan dan pertukaran data yang ditulis dalam JavaScript. Dari strukturnya, XML menggunakan tags yang didefinisikan oleh pembuatnya, sedangkan JSON menyimpan data dalam format key-value. JSON hanya mendukung tipe data primitif, sedangkan XML dapat mendukung tipe data kompleks, seperti bagan, grafik, dan lain-lain. JSON mendukung penggunaan array, sementara XML tidak.
2. XML adalah singkatan dari  Extensible Markup Language, sedangkan HTML adalah singkatan dari  Hypertext Markup Language. XML lebih berfokus pada penyimpanan serta pertukaran data, sedangkan HTML berfokus pada penampilan data pada web page. XML menggunakan tags yang didefinisikan oleh pembuatnya dan disesuaikan dengan data yang ada, berbeda dengan HTML, di mana tags yang digunakan sudah ada sebelumnya dan lebih digunakan untuk menandakan struktur tampilan pada webpage. Tags pada XML wajib memiliki closing tag, sementara pada HTML tidak wajib memiliki closing tag. XML bersifat case sensitive, sedangkan HTML tidak.

referensi:
https://www.geeksforgeeks.org/difference-between-json-and-xml/
https://byjus.com/free-ias-prep/difference-between-xml-and-html/
https://www.monitorteknologi.com/perbedaan-json-dan-xml/
https://www.javatpoint.com/html-vs-xml