from django import forms
from django.forms.widgets import Textarea
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    class Meta: 
        model = Note
        fields = "__all__"
    to_who = forms.CharField(label='To')
    from_who = forms.CharField(label='From')